﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HealthPlus.Models;
using HealthPlus.Repositorio;

namespace HealthPlus.Service
{
    public class AgendaService:ServiceBase<Agenda>
    {
        protected readonly AgendaRepository agendaRepository = new AgendaRepository();

        public IEnumerable<Medico> GetByMedico(string nome)
        {
          return  agendaRepository.GetByMedico(nome);
            
        }
    }
}