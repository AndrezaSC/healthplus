﻿using System;
using System.Collections.Generic;
using HealthPlus.Repositorio;

namespace HealthPlus.Service
{
    public class ServiceBase<TEntity> : IDisposable where TEntity : class
    {
        public readonly RepositoryBase<TEntity> repositoryBase = new RepositoryBase<TEntity>();
        public void Add(TEntity entity)
        {
            repositoryBase.Add(entity);
        }
        //public void AddRange(IEnumerable<TEntity> entity)
        //{
        //    repositoryBase.AddRange(entity);
        //}
        public void Update(TEntity entity)
        {
            repositoryBase.Update(entity);
        }
        public void Remove(TEntity entity)
        {
            repositoryBase.Remove(entity);
        }
        public TEntity GetById(int Id)
        {
            return repositoryBase.GetById(Id);
        }
        public IEnumerable<TEntity> GetAll()
        {
            return repositoryBase.GetAll();
        }

        public void Dispose()
        {
            repositoryBase.Dispose();
        }
    }
}