﻿using System.Collections.Generic;
using HealthPlus.Repositorio;
using HealthPlus.Models;
using System;

namespace HealthPlus.Service
{
    public class MedicoService:ServiceBase<Medico>
    {
        public readonly MedicoRepository medicoRepository = new MedicoRepository();

        public void AddRange(IEnumerable<Medico> listaMedicos)
        {
            medicoRepository.AddRange(listaMedicos);
        }
        public IEnumerable<Especialidade> GetListaEspecialidade(int[] lista)
        {
            return medicoRepository.GetByIDEsp(lista);
        }


    }
}