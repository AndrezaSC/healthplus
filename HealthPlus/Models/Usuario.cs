﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HealthPlus.Models
{
    public class Usuario
    {
        
        public int Id { get; set; }
        [Required]
        [Display(Name ="Nome")]
        public string Nome { get; set; }
        [Required]
        [Display(Name ="Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name ="Senha")]
        public string Senha { get; set; }

        
    }
}