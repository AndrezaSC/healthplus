﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HealthPlus.Models
{
    public class Agenda
    {
        public Agenda()
        {
            this.ListaMedicos = new HashSet<Medico>();
        }
        public int Id { get; set; }

        [Required]
        [Display(Name ="Data")]
        public DateTime Data { get; set; }

        public int MedicoId { get; set; }

        public virtual ICollection<Medico> ListaMedicos { get; set; }
        public virtual ICollection<Hospital> ListaHospitais  { get; set; }



    }
}