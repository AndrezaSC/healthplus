﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HealthPlus.Models
{
    public class Medico
    {
        
        public int Id { get; set; }
        [Required]
        [Display(Name = "CRM")]
        public string Crm { get; set; }
        [Required]
        [Display(Name = "Telefone")]
        public string Telefone { get; set; }
        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }
        [Required]
        public bool Ativo { get; set; }

        

        //public int EspecialidadeId { get; set; }

        public virtual IList<Especialidade> Especialidades { get; set; }
        

        
        

    }
}