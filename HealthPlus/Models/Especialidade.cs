﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HealthPlus.Models
{
    public class Especialidade
    {


        public int Id { get; set; }
        public string Tipo { get; set; }


        public virtual ICollection<Medico> Medicos { get; set; }
        
    }
}