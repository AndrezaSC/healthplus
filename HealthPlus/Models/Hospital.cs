﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace HealthPlus.Models
{
    public class Hospital
    {

        public int  Id { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Required]
        [Display(Name = "Horario de Abertura")]
        public DateTime HorarioDeAbertura { get; set; }

        [Required]
        [Display(Name = "Horario de Fechamento")]
        public DateTime HorarioFechamento { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string Nome { get; set; }

        [Required]
        [Display(Name = "Contato")]
        public string Contato { get; set; }

        
        
        public virtual Agenda Agenda { get; set; }
        

    }
}