﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using HealthPlus.Context;
using System.Data.Entity.Validation;

namespace HealthPlus.Repositorio
{
    public class RepositoryBase<TEntity> : IDisposable where TEntity : class
    {
        public readonly HealthPlusContexto Db = new HealthPlusContexto();
        public void Add(TEntity entity)
        {



            Db.Set<TEntity>().Add(entity);
            Db.SaveChanges();
        }


        public void Update(TEntity entity)
        {
            Db.Entry(entity).State = EntityState.Modified;
            Db.SaveChanges();
        }
        public void Remove(TEntity entity)
        {
            Db.Set<TEntity>().Remove(entity);
            Db.SaveChanges();
        }
        public TEntity GetById(int Id)
        {
            return Db.Set<TEntity>().Find(Id);
        }
        public IList<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }

        public void Dispose()
        {
            Db.Dispose();
            
        }
    }
}