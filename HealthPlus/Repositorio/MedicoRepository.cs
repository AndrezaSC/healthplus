﻿using System.Collections.Generic;
using System.Linq;
using HealthPlus.Models;

namespace HealthPlus.Repositorio
{
    public class MedicoRepository:RepositoryBase<Medico>
    {
        public IEnumerable<Medico> GetByNome(string nome)
        {
            return Db.Medicos.Where(p => p.Nome.Contains(nome)).ToList();
        }

        public void AddRange(IEnumerable<Medico> espMedico)
        {
              Db.Set<Medico>().AddRange(espMedico);
        }
        public IEnumerable<Medico> GetByIDEsp(int[] lista)
        {

            var ListaPreenchida = Db.Especialidades.Where(x => lista.Contains(x.Id)).ToList();
            Db.Medicos.AddRange(ListaPreenchida);
            return Db.Medicos.Where(x => ListaPreenchida.Contains(x.Especialidades));
            
        }
    }
}