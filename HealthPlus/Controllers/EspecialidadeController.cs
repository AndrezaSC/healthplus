﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthPlus.Service;
using HealthPlus.Models;

namespace HealthPlus.Controllers
{
    public class EspecialidadeController : Controller
    {
        public readonly EspecialidadeService especialidadeService = new EspecialidadeService();
        // GET: Especialidade
        public ActionResult Novo()
        {
            return View(new Especialidade());
        }

        [HttpPost]
        public ActionResult Novo(Especialidade e)
        {
            if (ModelState.IsValid)
            {
                especialidadeService.Add(e);
            }
            return View(e);
        }
    }
}