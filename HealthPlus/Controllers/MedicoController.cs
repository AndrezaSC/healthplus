﻿using HealthPlus.Context;
using HealthPlus.Models;
using HealthPlus.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace HealthPlus.Controllers
{
    public class MedicoController : Controller

    {
        protected readonly EspecialidadeService especialidadeService = new EspecialidadeService();
        protected readonly MedicoService medicoService = new MedicoService();
        public readonly HealthPlusContexto Db = new HealthPlusContexto();


        // GET: Medico
        public ActionResult Index()
        {
            var m = medicoService.GetAll();
            return View(m);
        }

        public ActionResult Detalhes(int id)
        {
            return View();
        }
        public ActionResult Novo()
        {
            ViewBag.Especialidades = especialidadeService.GetAll().ToList();
            return View();
        }

        [HttpPost]
        //[ReponseType(typeof(Medico))]
        
        public ActionResult Novo([Bind(Include = "Id,Tipo")]Medico m,int[] espe)
        {
            //Recupero dados do Formulario
            var lstEsp = Request.Form["chkEsp"];
            if (!string.IsNullOrEmpty(lstEsp))
            {
                //Converto para inteiro e insiro em uma array
                espe = lstEsp.Split(',').Select(Int32.Parse).ToArray();
                
                if (espe.Count() >0)//Verifico se tem +0
                {
                    var PostsMedicos = medicoService.GetListaEspecialidade(espe).ToList();
                    //var PostMedicos = Db.Especialidades.Where(w => espe.Contains(w.Id)).ToList();
                    medicoService.AddRange(PostsMedicos);
                    
                    //var PostMedico = especialidadeService.GetListaEspecialidade(splEsp);
                    //especialidadeService.AddRange(PostMedico);
                }
            }

            if (ModelState.IsValid)
            {


                Db.Medicos.Add(m);
                Db.SaveChanges();
                //medicoService.Add(m);

                return RedirectToAction("Index");
            }
            return View(m);
        }

       
        public ActionResult Editar(int id)
        {
           Medico medico= medicoService.GetById(id);


            return View(medico);
        }
        [HttpPost]
        public ActionResult Editar(Medico medico)
        {

            medicoService.Update(medico);
            return RedirectToAction("Index");
        }

 

        
    

    }
}