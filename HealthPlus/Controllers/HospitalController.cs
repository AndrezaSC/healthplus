﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HealthPlus.Models;
using HealthPlus.Service;

namespace HealthPlus.Controllers
{
    public class HospitalController : Controller
    {
        //Instanciando o Service do Hospital
        protected readonly HospitalService hpService = new HospitalService();
        // GET: Hospital

        public ActionResult Index()
        {
         var v=   hpService.GetAll();
            return View(v);
        }
        public ActionResult Novo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Novo(Hospital h)
        {
            if (ModelState.IsValid)
            {
                hpService.Add(h);
            }
            return View("Index");
        }
        
        
    }
}