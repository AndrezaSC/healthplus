﻿using System.Data.Entity;
using HealthPlus.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
using HealthPlus.EntityConfig;

namespace HealthPlus.Context
{
    public class HealthPlusContexto: DbContext
    {
        public HealthPlusContexto():base("HealthPlusDBFinal")
        {
        
        }

        #region DbSET
        
        public DbSet<Hospital> Hospitais { get; set; }
        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Agenda> Agendas { get; set; }
        public DbSet<Especialidade> Especialidades { get; set; }
        //public DbSet<EspecialidadeMedico> EspecialidadeMedicos { get; set; }


        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new EspecialidadeConfig());
            modelBuilder.Configurations.Add(new AgendaConfig());
            modelBuilder.Configurations.Add(new MedicoConfig());
            modelBuilder.Configurations.Add(new HospitalConfig());
            modelBuilder.Configurations.Add(new UsuarioConfig());
            base.OnModelCreating(modelBuilder);
        }
    }
}