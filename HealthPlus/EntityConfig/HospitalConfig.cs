﻿using HealthPlus.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace HealthPlus.EntityConfig
{
    public class HospitalConfig:EntityTypeConfiguration<Hospital>
    {
        public HospitalConfig()
        {
            ToTable("Hospital");
            HasKey(a => a.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            //HasRequired(a => a.Agenda).WithMany(h => h.ListaHospitais);
        }
    }
}