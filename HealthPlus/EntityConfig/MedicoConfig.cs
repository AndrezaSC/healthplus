﻿using HealthPlus.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace HealthPlus.EntityConfig
{
    public class MedicoConfig:EntityTypeConfiguration<Medico>
    {
        public MedicoConfig()
        {
            ToTable("Medico");
            HasKey(a => a.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            HasMany(e => e.Especialidades).WithMany(m => m.Medicos);
            
            


            
        }
    }
}